package com.university.midtermproject

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query




interface MovieService {

    @GET("movie/popular")
    fun getRequest(
        @Query("api_key") apiKey: String = "5d181a3c789a0384ebe24d9852dec8a6",
        @Query("page") page: Int
    ): Call<MoviesResponse>
}