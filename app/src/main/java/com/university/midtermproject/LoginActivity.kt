package com.university.midtermproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        Login.setOnClickListener{
            Login()
        }

    }

    private fun Login() {
        val email = loginEmail.text.toString()
        val password = loginPassword.text.toString()
        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Please enter a valid email and password", Toast.LENGTH_SHORT).show()
            return
        }

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener{
                if (it.isSuccessful){
                    val intent = Intent(this, MovieList::class.java)
                    startActivity(intent)
                    Log.d("Login", "Successfuly logged in user with UID: ${it.result?.user?.uid}")


                }
                return@addOnCompleteListener


            }
            .addOnFailureListener{
                Log.d("Login", "Failed to Login User: ${it.message}")
            }


    }

    private fun init(){
        Register.setOnClickListener {
            openSecondActivity()
        }
    }

    private fun openSecondActivity(){
        val intent = Intent(this, Registration::class.java)
        startActivity(intent)
    }

}
